<?php
require_once(dirname(__FILE__) . "/config.php");
function redirect($param){
    if(isset($param)){
        header("Location: " . "/public.php" . "?error=" . $param);
        exit;
    }
}
function redirect(){
    header("Location: " . "/public.php");
    exit;
}

    require_once(dirname(__FILE__) . "/module/csv_manager.php");
    require_once(dirname(__FILE__) . "/module/validate_module.php");

    $manager = new CSVManager('metadata/data.csv');
    $validator = new Validator();
    $csv_data = $manager->getCsvArray();

    $email = $_POST["email"];
    $email_validate_flag = $validator->is_email($email);
    if($email_validate_flag){
        error('emailのフォーマットではありません');
    }

    // 存在するEmailかどうか

    $old_password = $_POST["old_password"];
    $match_flag = $manager->checkEmailAndPasswordMatch(
        $email, $old_password, $csv_data);
    if(!$match_flag){
        $msg =  'Emailと過去のパスワードが一致しません';
        error($msg);
    }
    $new_password = $_POST["new_password"];
    $new_password_validate = $validator->is_normal_width($new_password);
    if(!$new_password_validate){
        error('パスワードに半角英数字以外の文字を含めることはできません');
    }
    $manager->updatePasswordFromEmail($email, $new_password, $csv_data);

    redirect();

