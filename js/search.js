$('#search').keyup(function(){
    if (!$(this).val()) {
        // 検索文字列無し
        $('#search_area tr').show();
    } else {
        // 検索文字列有り
        $('#search_area tr').hide();
        $('#search_area tr:contains(' + this.value + ')').show();
    }
});
