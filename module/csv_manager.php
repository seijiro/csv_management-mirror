<?php

ini_set("auto_detect_line_endings", true);

class CSVManager{
    private $filename;
    private $fp;

    public function __construct($filename){
        $this->filename = $filename;
        $this->fp   = fopen($this->filename, "r+");
        if($this->fp === FALSE){
            throw new Exception('not found csv file' . $this->filename);
        }
    }

    public function __destruct() {
        fclose($this->fp);
    }

    public function getCsvArray(){
        $csv = array();
        while (($data = fgetcsv($this->fp, 0, ",")) !== FALSE) {
          $csv[] = $data;
        }
        return $csv;
  }

  public function updatePasswordFromEmail($email, $new_password, $csv){
    $success = FALSE;
    foreach($csv as $key => $colum){
        if($colum[0] === $email){
            $colum[1] = $new_password;
            ftruncate ($this->fp, 0);
            $csv[$key] = $colum;
            $success = TRUE;
        }
    }
    // FIXME r+の状態で書き込むと16進数で書き込まれるので取り急ぎこちらにて対応
    if($success){
        fclose($this->fp);
        $this->fp   = fopen($this->filename, "a");

        $data = $this->csv_to_string($csv);
        fwrite($this->fp, $data);
        fclose($this->fp);
        $this->fp = fopen($this->filename, "r+");
    }
  }

  private function csv_to_string(array $csv_data){
    $buf = array();
    foreach($csv_data as $data){
        $string = implode('","', $data);
        $string =  '"' . $string . '"';
        array_push($buf, $string);
    }

    $tmp = implode("\n", $buf);
    return $tmp;
  }

  public function checkEmailAndPasswordMatch($email, $password, $csv){
    $success = FALSE;
    foreach($csv as $key => $colum){
        if($colum[0] === $email){
            if((string)$colum[1] == (string)$password){
                return TRUE;
            }
        }
    }
    return FALSE;
  }

  private function encloseDoubleQuotation(array $array){
    $buf = array();
    $enclosure = "\"";
    foreach($array as $key => $colum){
        $colum[0] = str_replace('"', '""', $colum[0]);
        $colum[0]= $enclosure . $colum[0] . $enclosure;

//        $colum[1] = '"' + $colum[1] + '"';
        $colum[1] = str_replace('"', '""', $colum[1]);
        $colum[1]= $enclosure . $colum[1] . $enclosure;

        array_push($buf, $colum);
    }
    return $buf;
  }
}
